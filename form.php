<!DOCTYPE html>
<html>
<head>
    <title>PHP File Handling</title>

    <style>
        .phpcoding {
            width:900px;
            margin:0 auto;
            background: #dddddd;
        }
        .header-option, .footer-option {
            background: #444444;
            color: #ffffff;
            text-align: center;
            padding:20px;
        }
        .maincontent {
            min-height:400px;
            padding:20px;
            display: flex;
            flex-direction:column;
            justify-content: center;
            margin:0 auto;
        }
        .header-option, .footer-option {
            margin:0;
        }

        p{
            margin:0;
        }

        input {
            box-sizing: border-box;
            width:300px;
            height:50px;
            padding-left: 35px;
            padding-right: 35px;
            padding-top: 10px;
            padding-bottom: 10px;
            margin-bottom:15px;
            background: lightseagreen;
            color: #ffffff;
            font-size:30px;
            text-align: center;
        }
            
         .sohel:hover {
            background: green;
             -webkit-transition: prop time;
             -moz-transition: prop time;
             -ms-transition: prop time;
             -o-transition: prop time;
             transition: all 0.5s;
        }

    </style>
</head>
<body>

<section class="phpcoding">
    <div class="header-option">
        <h2>PHP Fundamentals</h2>
    </div>

    <div class="maincontent">
        <hr/>
            <span style="float: right;">
              Time: <?php date_default_timezone_set("Asia/Dhaka");
                echo date("h:i:sa");
                ?>
            </span>

        </span>
            <span>PHP Time and Date: <?php echo date("d-m-Y")." ".date("l");
                ?></span>
        <hr/>

        <form action="Calc.php" method="post">
            <input type="number" name="input1">
            <br/>
            <input type="number" name="input2">
            <br/>
            <input class="sohel" type="submit" value="SUBMIT" name="submit">
            <br/>
        </form>

    </div>

    <div class="footer-option">
        <p>&copy; <?php echo date("Y");?> makpie.com</p>
        <h2>www.makpie.com</h2>
    </div>

</section>

</body>
</html>