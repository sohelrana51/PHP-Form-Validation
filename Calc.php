<!DOCTYPE html>
<html>
<head>
    <title>PHP File Handling</title>

    <style>
        .phpcoding {
            width:900px;
            margin:0 auto;
            background: #dddddd;
        }
        .header-option, .footer-option {
            background: #444444;
            color: #ffffff;
            text-align: center;
            padding:20px;
        }
        .maincontent {
            min-height:400px;
            padding:20px;

        }
        .header-option, .footer-option {
            margin:0;
        }

        p{
            margin:0;
        }
    </style>
</head>
<body>

<section class="phpcoding">
    <div class="header-option">
        <h2>PHP Fundamentals</h2>
    </div>

    <div class="maincontent">
        <hr/>
            <span style="float: right;">
              Time: <?php date_default_timezone_set("Asia/Dhaka");
                echo date("h:i:sa");
                ?>
            </span>

        </span>
            <span>PHP Time and Date: <?php echo date("d-m-Y")." ".date("l");
                ?></span>
        <hr/>

        <?php

        class Calc
        {
            public $num1 = '';
            public $num2 = '';

            public function setnumber ($finput1 = '', $finput2 = '')
            {
                $this->num1 = $finput1;
                $this->num2 = $finput2;
            }

            public function getnumber ()
            {
                $first_num = $this->num1;
                $second_num = $this->num2;
                $mul = $first_num * $second_num;
                return $mul;
            }

        }

        $fmul = new Calc();
        $fmul->setnumber($_POST['input1'],$_POST['input2']);
        echo $fmul->getnumber();

        ?>

    </div>

    <div class="footer-option">
        <p>&copy; <?php echo date("Y");?> makpie.com</p>
        <h2>www.makpie.com</h2>
    </div>

</section>

</body>
</html>